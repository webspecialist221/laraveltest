<?php

namespace App\Repositories\SqlRepositories;

use App\Models\Link;
use App\Models\User;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Interfaces\UserRepository;

class UserSqlRepository implements UserRepository
{
    // Login account
    public function login($request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->role === 'admin') {
                $success['token'] =  $user->createToken('MyApp')->plainTextToken;
                $success['name'] =  $user->name;
            } elseif ($user->role === 'user' and $user->status === 1) {
                $success['token'] =  $user->createToken('MyApp')->plainTextToken;
                $success['name'] =  $user->name;
            } else {
                return response()->json('Your Account is not Verified. Please Verify first then login again', 400);
            }
            $response = [
                'success' => true,
                'data'    => $success,
            ];

            return response()->json($response, 200);
        } else {
            return response()->json('Invalid Credentials', 400);
        }
    }

    // Send url through email for account creation
    public function mail($request)
    {
        $details = [
            'title' => 'abc',
            'body' => 'body',
            'url' => 'localhost:8000/api/register',
            'code' => rand(10000,100000)
        ];

        $url = Link::create([
            'url' => $details['url'],
            'code' => $details['code']
        ]);

        if($url){
            Mail::to($request->email)->send(new TestMail($details));
            return "Email Sent";
        }else{
            return "Email Not Sent";
        }
    }

    // Register account
    public function register($request)
    {
        $links = Link::where('url','localhost:8000/api/register')->where('code', $request->code)->first();
        if($links){
            $code = rand(10000,100000);
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => 'user',
                'code' => $code,
                'status' => 0
            ]);

            $details = [
                'title' => 'abc',
                'body' => 'body',
                'url' => 'localhost:8000/api/verify-account',
                'code' => $code
            ];

            Mail::to($request->email)->send(new TestMail($details));

            $response = [
                'success' => true,
                'message'    => 'A verification code sent to your email. check email and verify the account',
            ];

            return response()->json($response, 200);
        }else{
            $response = [
                'success' => false,
                'message'    => 'the code you provided is not correct.Check Again in your mailbox',
            ];
            return response()->json($response, 200);
        }
    }

    // Verify account
    public function verify($request)
    {
        $user = User::where('email', $request->email)->first();
        if($user->code == $request->code){
            $user->status = 1;
            $user->save();
            return response()->json('Your Account is verified and Login now', 200);
        }else{
            return response()->json('Invalid Email/Verification Code', 400);
        }
    }
}
