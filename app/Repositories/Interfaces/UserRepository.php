<?php

namespace App\Repositories\Interfaces;

interface UserRepository
{
    // Login
    public function login($request);

    // Send Link via mail for signup
    public function mail($request);

    // Register the user
    public function register($request);

    // Verify account
    public function verify($request);
}
