<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\User;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Interfaces\UserRepository;

class UserController extends Controller
{

    public function register(Request $request, UserRepository $repo)
    {
        return $repo->register($request);
    }

    public function login(Request $request, UserRepository $repo)
    {
        return $repo->login($request);
    }

    public function mail(Request $request, UserRepository $repo)
    {
        return $repo->mail($request);
    }

    public function verify(Request $request, UserRepository $repo)
    {
        return $repo->verify($request);
    }
}
