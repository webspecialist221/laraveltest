<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);
Route::post('verify-account', [UserController::class, 'Verify'])->name('verify.account');



Route::middleware('auth:sanctum')->group( function () {
    Route::post('mail', [UserController::class, 'mail'])->name('mail');
    Route::get('profile',function(){
        return 'User Profile Page';
    });
});
